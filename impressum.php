<?php
include "dbh.php";
?>
<!-- ------------------------------------------------------------------------------- -->
<!DOCTYPE html>
<html lang="de-DE">
<head>
<title>project-torpedo.com | </title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<meta name="keywords" content="Projekt Torpedo, Webspace zum Üben" />
<meta name="description" content="Webprogrammierungsprojekt project-torpedo.com" />
<link rel=stylesheet type="text/css" href="project-torpedo.css" media="screen"/> 

<!-- GET http://project-torpedo.com/favicon.ico 404 (Not Found)  ??-->
<link rel="shortcut icon" href="#" />

<!-- Javascript: Funktionen und Webkomponente -->
<script src="grid.js" type="text/javascript"></script>
	<!-- <link rel="import" href="basketPopup.php"/> -->

</head>
    
<body>
<header id="main_header">
   
   <!--Logo -->
  	<a href="index.php">
        <img src="pictures/logo.jpg" id="logo" alt="Logo" title="PROJECT-TORPEDO.COM"/>
    </a> 
 
		
  <!-- H1 Überschrift-->
  <div class="typewriter" id="h1">
		<h1> <strong> PROJECT-TORPEDO.COM  </strong> -  <em>Trainier' wie eine Rakete!</em> </h1>
  </div>
		
</header>
     
<!-- ----------------------------------------------------------------------------------- -->
<!-- Navigation: ...... -->
<!-- ----------------------------------------------------------------------------------- -->
  
<!-- Navigation der Seite Index-->
<aside>
  <nav>
    <ul>
      <li><a href="seite_1.html">Sport und Fitness</a></li>
      <li><a href="seite_2.html">Unsere App</a></li>
      <li><a href="seite_3.html">Übungen</a></li>
	  <li><a href="seite_4.html">Ernährung</a></li>
	  <li><a href="seite_5.html">Kalorienrechner</a></li>
	  <li><a href="seite_6.html">Infos III</a></li>
	  <li><a href="seite_7.html">Func II</a></li>
<!--  <li><a href="seite_6.html">Unere Traingsapp</a></li>
	  <li><a href="seite_7.html">Kalorienrechner</a></li>
	  <li><a href="seite_8.html">Literatur</a></li>
	  <li><a href="seite_9.html">usw.</a></li>
	  <li><a href="wordpress/">WordPress offline</a></li>
	  <li><a href="wordpress/CMS/">WordPress online</a></li>
-->
	  <!-- NEW REGISTER --> 	
		<form id="newregister_form">
			
			<label>Noch nicht registriert?</label> </br>
			<input id="registerBtn" type="button" value="Jetzt registrieren">
		 </form>
		
	  
	  
	  <!-- LOGIN --> 	
		 <form id="login_form">
			<label>Username:</label>
			<input id="userTB" type="text" name="user">  </br>
			<label>Passwort: </label>
			<input id="passwordTB" type="password" name="password"> 		
			<input id="loginBtn" type="button" name="login" value="Login">		
		  </form>
	  
	  <!-- SUCHE -->
	  <form id="search_form">
		<input id="searchTB" type="text" name="search">
		<input id="searchBtn" type="button" value="Suche"> 
	  </form>
	  
		
    </ul>
	
  </nav>
  

  
</aside>



<!-- ------------------------------------------------------------------------------- -->    
<!-- ------------------------------------------------------------------------------- -->
                            <!-- Inhalt der Seite Impressum -->
<!-- ------------------------------------------------------------------------------- -->
<!-- ------------------------------------------------------------------------------- -->
    
<main>
    
  <h2>Impressum</h2>
  
<!-- Wir  	AUSKOMMENTIERT --> 
  <article>
	
	<h3>Inhaber</h3>
    <p>
	Kunc, Sven </br>
	Schleier, Erwin </br>
	Saado, Dlschad </br>
	Menabdishvilli, Vakhtangi 
    </p>
	
  </article>
    
<!-- Beispielimpressum -->  
   <article> 
  <!--  
  <h3>Postanschrift          AUSKOMMENTIERT   </h3>
    <p>
	Straße 12 	<br>
	90489 Nürnberg	<br>
	Tel. 0911-0000-01 <br>
	Fax. 0911-0000-02 <br>
	E-Mail: info@toymodels.de <br>
	Internet: www.toymodels.de <br>
	</p>
	<p>
	Handelsregister: Amtsgericht Scheinstadt Abt. XYZ HRA 1234 <br>
	Umsatzsteuer-ID: DE 123456789 Zuständige Kammer: IHK Nürnberg/Mittelfranken <br>
	</p> 
	--> 
   </article>

<!-- Haftungsausschlüsse -->    
  <article>
    <h3>Haftung für Inhalte</h3>
    <p>Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen.</p>
    <p>Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.</p>
  </article>
  <article>
    <h3>Haftung für Links</h3>
    <p>Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar.</p>
    <p>Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.</p>
  </article>
  <article>
    <h3>Urheberrecht</h3>
    <p>Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet.</p>
    <p>Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.</p>
  </article>
  <address>
    <p><i>Quelle: <a href="https://www.e-recht24.de" target="blank">eRecht24</a></i></p>
  </address>
    
</main>
    
  !-- ------------------------------------------------------------------------------- -->
    <!-- ------------------------------------------------------------------------------- -->
                    <!--untere Navigation der Seite Impressum -->

  <footer>
    <ul>
      <!-- <li><a href="about.php">Über uns</a></li>
      <li><a href="#">Impressum</a></li>-->
	  <li><a href="about.html">Über uns</a></li>
      <li><a href="impressum.html">Impressum</a></li>
	  <!-- <li><a href="temp.json">format</a></li>
      <li><a href="#">Sonstiges?! (weitere Fußzeilenlinks)</a></li> -->
    </ul>
  </footer>

</body>
</html>