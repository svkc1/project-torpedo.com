<?php
include "dbh.php";
?>
<!-- ------------------------------------------------------------------------------- -->
<!DOCTYPE html>
<html lang="de-DE">
<head>
<title>project-torpedo.com | </title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<meta name="keywords" content="Projekt Torpedo, Webspace zum Üben" />
<meta name="description" content="Webprogrammierungsprojekt project-torpedo.com" />
<link rel=stylesheet type="text/css" href="project-torpedo.css" media="screen"/> 

<!-- GET http://project-torpedo.com/favicon.ico 404 (Not Found)  ??-->
<link rel="shortcut icon" href="#" />

<!-- Javascript: Funktionen und Webkomponente -->
<script src="grid.js" type="text/javascript"></script>
	<!-- <link rel="import" href="basketPopup.php"/> -->

</head>
    
<body>
<header id="main_header">
   
   <!--Logo -->
  	<a href="index.php">
        <img src="pictures/logo.jpg" id="logo" alt="Logo" title="PROJECT-TORPEDO.COM"/>
    </a> 
 
		
  <!-- H1 Überschrift-->
  <div class="typewriter" id="h1">
		<h1> <strong> PROJECT-TORPEDO.COM  </strong> -  <em>Trainier' wie eine Rakete!</em> </h1>
  </div>
		
</header>
     
<!-- ----------------------------------------------------------------------------------- -->
<!-- Navigation: ...... -->
<!-- ----------------------------------------------------------------------------------- -->
  
<!-- Navigation der Seite Index-->
<aside>
  <nav>
    <ul>
      <li><a href="seite_1.html">Sport und Fitness</a></li>
      <li><a href="seite_2.html">Unsere App</a></li>
      <li><a href="seite_3.html">Übungen</a></li>
	  <li><a href="seite_4.html">Ernährung</a></li>
	  <li><a href="seite_5.html">Kalorienrechner</a></li>
	  <li><a href="seite_6.html">Infos III</a></li>
	  <li><a href="seite_7.html">Func II</a></li>
<!--  <li><a href="seite_6.html">Unere Traingsapp</a></li>
	  <li><a href="seite_7.html">Kalorienrechner</a></li>
	  <li><a href="seite_8.html">Literatur</a></li>
	  <li><a href="seite_9.html">usw.</a></li>
	  <li><a href="wordpress/">WordPress offline</a></li>
	  <li><a href="wordpress/CMS/">WordPress online</a></li>
-->
	  <!-- NEW REGISTER --> 	
		<form id="newregister_form">
			
			<label>Noch nicht registriert?</label> </br>
			<input id="registerBtn" type="button" value="Jetzt registrieren">
		 </form>
		
	  
	  
	  <!-- LOGIN --> 	
		 <form id="login_form">
			<label>Username:</label>
			<input id="userTB" type="text" name="user">  </br>
			<label>Passwort: </label>
			<input id="passwordTB" type="password" name="password"> 		
			<input id="loginBtn" type="button" name="login" value="Login">		
		  </form>
	  
	  <!-- SUCHE -->
	  <form id="search_form">
		<input id="searchTB" type="text" name="search">
		<input id="searchBtn" type="button" value="Suche"> 
	  </form>
	  
		
    </ul>
	
  </nav>
  

  
</aside>



    <!-- ------------------------------------------------------------------------------- -->
    <!-- ------------------------------------------------------------------------------- -->
                        <!-- Inhalt der Seite Artikeleinzelansicht -->
    <!-- ------------------------------------------------------------------------------- -->
    <!-- ------------------------------------------------------------------------------- -->
    
<main>
    
 <h2>Einzelanischt</h2>
     
	 <?php
	 
	//Message-Bereich
    if (isset($_SESSION['message']) )
       {
            //Ausgabe Artikel x erfolgreich aktualisiert!
            echo "<article class='message'>";
            echo "<strong>";
            echo $_SESSION['message'] ;
            echo "</strong>";
            echo "</article>";
            
            unset ($_SESSION['message']);
       }
	 
	 
    echo "<article>";
       
    $set = "set";
    //falls id_torpedo_artikel übergeben
    if (isset ($_GET['id_torpedo_artikel'])) {
        
        //übergebene id_torpedo_artikel
        $id_torpedo_artikel = $_GET['id_torpedo_artikel'];
	
        //SQL
        $sqlget = "SELECT * FROM torpedo_artikel WHERE id_torpedo_artikel = '$id_torpedo_artikel' ";
        $sqldata = $pdo -> query($sqlget) or die ("Bad Query: $sqlget");
        $row = $sqldata->fetch(PDO::FETCH_ASSOC);        
    
        //Artikelüberschrift
        echo "<h3>";
        echo $row ['Title'];
		echo $row ['id_torpedo_artikel'];
        echo "</h3>";
    
        //Funktionsaufruf für Bilderauswahl
        //$GruppenNr = $row ['GruppenNr'];
        //bildAuswahl($GruppenNr);
        
        //Artikelbeschreibung
        echo "<p>";
        echo $row ['Subtitle'];
        echo "</p>";
        echo "<p>";
        echo $row ['Text'];
        echo "</p>";
        
    }
       
    //falls keine id_torpedo_artikel übergeben   
    else {
        header('Location: keineid_torpedo_artikelübergeben.php');
    }
          
    //Bestellen / Zum Warenkorb hinzufügen
    //echo "<form action='dbh.php?id_torpedo_artikel={$row ['id_torpedo_artikel']}&set=true' method='post'>";
    //echo "<input type='number' name='amount' min='1' max={$row ['Bestandsmenge']} value='1' placeholder='Menge eingeben'/>";
    //echo "<input type='submit' name='id_torpedo_artikel' value='Zum Warenkorb hinzufügen'></input>";
    //echo "</form>";       

	
    echo "</article>";
	
	?>
	
</main>

    
<!-- ------------------------------------------------------------------------------- -->
    <!-- ------------------------------------------------------------------------------- -->
                    <!--untere Navigation der Seite Impressum -->

  <footer>
    <ul>
      <!-- <li><a href="about.php">Über uns</a></li>
      <li><a href="#">Impressum</a></li>-->
	  <li><a href="about.html">Über uns</a></li>
      <li><a href="impressum.html">Impressum</a></li>
	  <!-- <li><a href="temp.json">format</a></li>
      <li><a href="#">Sonstiges?! (weitere Fußzeilenlinks)</a></li> -->
    </ul>
  </footer>

</body>
</html>