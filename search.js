//Suchfunktion
function search() { 
   var searchString = document.getElementById('searchString').value; // Suchwort auslesen 

   var result = document.getElementById('pageContent').innerHTML.match(searchString); // in pageContent nach dem Suchwort suchen 
   if (result) 
    alert("Der Suchbegriff \""+searchString+"\" wurde " + result.length + " mal gefunden."); 
   else 
    alert("Der Suchbegriff \""+searchString+"\" wurde nicht gefunden."); 
  }