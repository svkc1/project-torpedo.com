<?php 
//include "functions.php"; wird von wp aufgerufen
?>
<!DOCTYPE html>

<!-- HEADER (body öffnen) -->
<?php get_header(); ?>

<header>
 <h1>Fakultät IN - Gruppenprojekt im FWPF 'Grundlagen CMS' im SS19 an der TH Nürnberg Georg Simon Ohm</h1>	
</header>

<!-- Menü hinzufügen -->
<!-- <?php wp_nav_menu(array( 'theme_location' => 'primary' )); ?> -->

<!-- <?php get_nav(); ?> -->

<aside>
 <nav class="menu">
  <ul>
	<?php
    wp_list_pages( array(
        'title_li' => ''	//Überschrift der Navigation verstecken
));
?>
  </ul>
 </nav> 
</aside>

<!-- ------------------------------------------------------------------------------------------->
<!-- ------------------------ ANFANG CONTENT und Hauptbereich des Impressums -------------------------->

<main>
    
  <h2>Impressum</h2>
  
<!-- Wir --> 
  <article>
	
	<h3>Inhaber/Studenten</h3>
    <p>
	Braun, Jakob <br> 
	Yavas, Ayberk <br> 
	Kunc, Sven <br>
    </p>
	
  </article>
    
<!-- Beispielimpressum -->  
   <article> 
  <h3>Postanschrift  </h3>
    <p>
	Technische Hochschule Nürnberg Georg Simon Ohm<br>
    <b>Fakultät Informatik</b><br>
	Keßlerplatz 12<br>
    90489 Nürnberg<br>
	Tel. 0911-0000-01 <br>
	Fax. 0911-0000-02 <br>
	E-Mail: info@example.de <br>
	Internet: www.example.de <br>
	</p>
	<p>
	Handelsregister: Amtsgericht Scheinstadt Abt. XYZ HRA 1234 <br>
	Umsatzsteuer-ID: DE 123456789 Zuständige Kammer: IHK Nürnberg/Mittelfranken <br>
	</p> 
	--> 
   </article>

<!-- Haftungsausschlüsse -->    
  <article>
    <h3>Haftung für Inhalte</h3>
    <p>Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen.</p>
    <p>Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.</p>
  </article>
  <article>
    <h3>Haftung für Links</h3>
    <p>Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar.</p>
    <p>Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.</p>
  </article>
  <article>
    <h3>Urheberrecht</h3>
    <p>Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet.</p>
    <p>Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.</p>
  </article>
  <address>
    <p><i>Quelle: <a href="https://www.e-recht24.de" target="blank">eRecht24</a></i></p>
  </address>
    
</main>


<!-- ------------------------------------------------------------------------------------------->
<!-- ------------------------ ENDE CONTENT und Hauptbereich der Startseite -------------------------->


<!-- SIDEBAR -->
<?php get_sidebar(); ?>


<!-- FOOTER (body und html schließen) -->
<?php get_footer(); ?>
