<?php
/*
Plugin Name: CubeCard (a Start Page Plugin)
Plugin URI: localhost/wordpress/
Version: 2.0
Author: Sven Kunc
Description: a 3D-InformationCube and a BusinessCard to display your personal WP content
*/


//Konstanten zur Speicherung der User- und Admineinstellungen 
const DOMAIN = 'cubecarddomain';									//Versuch der Internationalisierung

const SETTING_NAME_AD_CARD = 'card_setting_ad';						//Titel Karte

const SETTING_RADIO_AD = 'radio_setting_ad';					     //DropDown Karte oder Würfel in den Admineinstellungen
const SETTING_RADIO_DEFAULT_AD = "Card";

const SETTING_NAME_AD ='cube_setting_ad';							//Titel Würfel
const SETTING_DEFAULT_AD = "Hallo und viel Spaß mit dem Plugin!";

const SETTING_NAME_AD1 = 'cube_setting_ad1';						//1
const SETTING_DEFAULT_AD1 = "1. Seite";
const SETTING_NAME_AD2 = 'cube_setting_ad2';						//2
const SETTING_DEFAULT_AD2 = "2. Seite";
const SETTING_NAME_AD3 = 'cube_setting_ad3';						//3
const SETTING_DEFAULT_AD3 = "3. Seite";
const SETTING_NAME_AD4 = 'cube_setting_ad4';						//4
const SETTING_DEFAULT_AD4 = "4. Seite";
const SETTING_NAME_AD5 = 'cube_setting_ad5';						//5
const SETTING_DEFAULT_AD5 = "5. Seite";
const SETTING_NAME_AD6 = 'cube_setting_ad6';						//6
const SETTING_DEFAULT_AD6 = "6. Seite";

const SETTING_CARD_OR_CUBE = 'default';								//Combobox Karte oder Würfel im Userprofil

///////////////////////////////////////////
//////////////////////////////////////////////
//////////////////////////////////////

//Klassenstruktur anhand des Vorlesungsbeispiels
class cubecard{

	
protected static $_instance = null; // Singleton instance
public static function get_instance() {
if (null === self::$_instance) {
self::$_instance = new self;
}
return self::$_instance;
}
protected function __clone() {} // Prevent singleton cloning
protected function __construct() {

	//// User Custom Profile Fields
	add_action( 'show_user_profile', array($this, 'add_profile_fields' ));
	add_action( 'edit_user_profile', array($this, 'add_profile_fields' ));
	// Save the Custom Fields
	add_action( 'personal_options_update', array($this, 'save_add_profile_fields' ));
	add_action( 'edit_user_profile_update', array($this, 'save_add_profile_fields' ));	
		
	//Translation Versuch
	//add_action( 'plugins_loaded', array(&$this, 'load_language_textdomain') );
	
	//Action-Hooks nach Vorlesungsbeispiel zum Hinzufügen und Registrieren der PluginEinstellungen
	add_action( 'plugins_loaded', array(&$this, 'plugins_loaded') );
	add_action('admin_init', array(&$this, 'admin_init'));
	add_action('admin_menu', array(&$this, 'admin_menu'));		
		
	//Ausgabe der Plugins an _posts=Schleifenbeginn	
	add_action( 'loop_start', array(&$this, 'print_cube') );			//Cube: 1. "Post" -> @loop_start der Startseite #is_home()
	add_action( 'loop_start', array(&$this, 'print_card') );			//Visitenkarte: Persönliches auf der Profilseite
	
	//Laden und Anhängen der Stylesheets an den Header
	add_action( 'wp_enqueue_scripts', array($this, 'frontend_styles' ));      //enqueuue scripts zum Anfügen in den head
		
	/*//use plugin features via shortcode #css VERSUCH(funktioniert)
	 add_shortcode('card',   array(&$this, 'frontend_styles') );
	 add_shortcode('card',   array(&$this, 'frontend_styles') );
	 add_shortcode('cube',   array(&$this, 'print_cube') );
	 add_shortcode('card',   array(&$this, 'print_card') );
	 */
	
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Versuch zur Internationalisierung 
function load_language_textdomain() {
	//echo get_locale(); 
	load_plugin_textdomain( DOMAIN, false, '/cube/languages' );
     
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Hinzufügen/Updaten zur User Meta
function save_add_profile_fields($user_id) {
	if ( !current_user_can( 'edit_user', $user_id ) ) { return false; }
	
		update_user_meta( $user_id, 'favorite_subject' ,$_POST['favorite_subject'] );
		//update_user_meta( $user_id, 'display_avatar', $_POST['display_avatar'] );
	
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Hinzufügen der ComboBox im Userprofil zur Auswahl ob Cube- oder Card-Darstellung
function add_profile_fields( $user ) { ?>

<h2><?php _e("Visitenkarte/-Infowürfel-Darstellungsoptionen", DOMAIN); //Business-Card and Info-Cube presentation settings"?></h2>
<table class="form-table">

<tr>
<th>
<label for="rank">
	<?php _e("Ihre gewünschte Darstellungsform: ", DOMAIN); //"Your desired presentation:"); 
	echo get_user_meta($user->ID, 'favorite_subject', true); ?>
</label>
</th>
<td>

<select name="favorite_subject" id="favorite_subject">
<?php $selected = get_user_meta($user->ID, 'favorite_subject', true); ?>
<option value="">Select</option>
<!-- ComboBox -->
<option value="card" <?php 
						if(get_user_meta($user->ID, 'favorite_subject', true) == 'card') 
						{
							echo 'selected'; 
							$selected = "card";	
							//echo "KARTE";
							//SETTING_CARD_OR_CUBE = 'card';							
						}
					?>>Visitenkarte auf der Startseite
</option>
<option value="cube" <?php 
						if(get_user_meta($user->ID, 'favorite_subject', true) == 'cube') 
						{	
							echo 'selected'; 
							$selected = "cube";
							//echo "WÜRFEL";
							//SETTING_CARD_OR_CUBE = 'cube';
						}
					?>>Infowürfel auf der Startseite
</option>
<!-- <option value="shortcode" <?php 
						if(get_user_meta($user->ID, 'favorite_subject', true) == 'shortcode') 
						{	
							echo 'selected'; 
							$selected = "shortcode";
							//echo "WÜRFEL";
							//SETTING_CARD_OR_CUBE = 'cube';
						}
					?>>Shortcodes: [card] oder [cube]
</option> -->
<option value="null" <?php if(get_user_meta($user->ID, 'favorite_subject', true) == 'null') echo 'selected'; ?>>-Keine Darstellung-</option>

</select>

</td>
</tr>

<!-- Beispiel und weitere Literatur: https://www.websitebravo.com/add-custom-user-profile-fields-wordpress/
<tr>
<th><label for="favorite_subject"><?php //_e("Display Avatar?"); ?></label></th>
<td>
<input type="checkbox" name="display_avatar" value="yes" <?php if(get_user_meta($user->ID, 'display_avatar', true)=='yes') echo 'checked="checked"'; ?> />Yes<br />
</td>
</tr>
-->

</table> 

<?php } //end  profile page settings cubecard

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Hinzufügen und Laden der Stylesheets in den Head
function frontend_styles() {
	$plugin_url = plugin_dir_url( __FILE__ );
	
	wp_enqueue_style( 'cube', $plugin_url . 'css/cube.css' ); //css für den cube einfügen
	wp_enqueue_style( 'card', $plugin_url . 'css/card.css' ); //css für die card einfügen
	
	wp_enqueue_script('cube', $plugin_url .'js/cube.js', array('jquery'), false, true); //javascript in den footer einfügen, letztes true
	wp_enqueue_script('card', $plugin_url .'js/card.js', array('jquery'), false, true); //javascript in den footer einfügen, letztes true
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Ausgabe der Visitenkarte, Prüfung am Anfang: Startseite? Karte im Userprofil ausgewählt? Eingeloggt und hat Instanz?
function print_card() {

$current_user = wp_get_current_user();

//Card-Plugin soll nur auf der Startseite des Users dargestellt werden
if (is_home())
{	
//falls  Karte ausgeählt
	if (get_user_meta($current_user->ID, 'favorite_subject', true) == 'card')
	//if(get_option(SETTING_RADIO_AD) == 'Card')
	{
		//falls User eingeloggt
		if ( is_user_logged_in() ) {    
		//Instanzprüfung
			if ( ($current_user instanceof WP_User) ) {
?>
	
	<!-- CARD -->
	
<section class="sectionCard">
	<?php 
	echo '<h2>'.esc_html('Herzlich Willkommen auf meiner Seite').'</h2>';
	//echo ('<h2>'.get_option( SETTING_NAME_AD ) . '</h2>'); 
	?>
	<h3><em><?php echo (esc_html('Für die Rückseite Karte anklicken'));//Click card to flip!</?></em></h3>
	<div class="cardscene scene--card">
		<div class="card">
			<!-- Visitenkarte Vorderseite-->
			<div class="card__face card__face--front"> 
			<?php
			// echo ('<p>'.get_option( SETTING_NAME_AD1 ) . '</p>');
		// echo ('<p>'.get_option( SETTING_NAME_AD2 ) . '</p>');
			// echo ('<p>'.get_option( SETTING_NAME_AD3 ) . '</p>');
			
			//Holen der User-Meta
			$user_id = get_current_user_id();
			$all_meta_for_user = get_user_meta($user_id);
			//
			//1. User Meta lesen
			//creat a map with personal information from the profile about the user, ignore null values
			$all_meta_for_user = array_map( function( $a ){ return $a[0]; }, get_user_meta( $user_id ) );
			// Get all user meta data for $user_id
			$meta = get_user_meta( $user_id );
			// Filter out empty meta data
			$meta = array_filter( array_map( function( $a ) {
				return $a[0];
			}, $meta ) );			
			//print_r(get_user_meta($user_id)); //print_r($meta); ////print_r($all_meta_for_user);
			//
			$last_name = $meta['last_name'];
			$first_name = $meta['first_name'];
			$description = $meta['description'];		
			$nickname = $meta['nickname'];	
			//	
			//2. User Data lesen 
			//-> Zuordnung
			$user_data = get_userdata($user_id);
			//
			$username = $user_data->user_login;
			$user_url = $user_data->user_url;
			$user_email = $user_data->user_email;
			//$username = $user_data->
			//$username = $user_data->
			
			
			//x. Print Visitenkarte Vorderseite
			echo '<p class="p_Front">'.
					'<strong>'.$first_name.'</strong>'." ".'<strong>'.$last_name.'</strong>'.'</br>'.
					"E-Mail: ".$user_email .'</br>'.
					"Homepage: ".$user_url .'</br>'.
					'</br>'.
					'<img>'.get_avatar( $current_user->ID, 32	).'</img>'.
					//" '$description' ". '</br>'.
					'<em>'."Sie nennen ihn auch ".$nickname.".".'</em>'.				
				'</p>';
				
			?>
			</div>
			<!-- Rückseite-->
			<div class="card__face card__face--back">
			<?php
			// echo ('<p>'.get_option( SETTING_NAME_AD4 ) . '</p>');
		// echo ('<p>'.get_option( SETTING_NAME_AD5 ) . '</p>');
			// echo ('<p>'.get_option( SETTING_NAME_AD6 ) . '</p>');
			
			//x. Print Visitenkarte Rückseite
			echo '<div class="p_Back" id="profile_picandtext_container">'.
			'<div id="profile_pic">'.'<img>'.get_avatar( $current_user->ID, 144	).'</img>'.'</div>'.
			'<h4>'.$username."'s Biographie:".'</h4>'.
			'<div id="profile_pic_text">'.$description.'</div>'.
			'</div>';
			        
    }
}
?>
			</div>
		</div>
	</div>
</section>
<?php
    } //end if-clause is_home()
  } //end if-clause ==card
} //end print_card()
 
 
 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//Ausgabe des Informationswürfels
function print_cube() {	
//	echo('<span style="background-color: red;">' .get_option( SETTING_NAME_AD ) . '</span>');

$current_user = wp_get_current_user();

//Cube-Plugin soll nur auf der Startseite des Users dargestellt werden
if (is_home())
{
	//Cube ausgewählt?
	if (get_user_meta($current_user->ID, 'favorite_subject', true) == 'cube')
	//if (get_option(SETTING_RADIO_AD) == 'Cube')
{
?>
<!-- CUBE -->
<section class="sectionCube">
<?php echo ('<h2>'.get_option( SETTING_NAME_AD ) . '</h2>'); ?>
 <h3><em><?php echo(esc_html('Auswahl treffen für die Würfelseite'));//Select a cube side!?> </em></h3>
<div class="scene">
  <div class="cube">
    <div class="cube__face cube__face--front"><?php
echo ('<h2>'.get_option( SETTING_NAME_AD1 ) . '</h2>');
?></div>
    <div class="cube__face cube__face--back"><?php
echo ('<h2>'.get_option( SETTING_NAME_AD3 ) . '</h2>');
?></div>
    <div class="cube__face cube__face--right"><?php
echo ('<h2>'.get_option( SETTING_NAME_AD2 ) . '</h2>');
?></div>
    <div class="cube__face cube__face--left"><?php
echo ('<h2>'.get_option( SETTING_NAME_AD4 ) . '</h2>');
?></div>
    <div class="cube__face cube__face--top"><?php
echo ('<h2>'.get_option( SETTING_NAME_AD5 ) . '</h2>');
?></div>
    <div class="cube__face cube__face--bottom"><?php
echo ('<h2>'.get_option( SETTING_NAME_AD6 ) . '</h2>');
?></div>
  </div>
</div>
<p class="radio-group">
  <label>
    <input type="radio" name="rotate-cube-side" value="front" checked /><?php echo (get_option( SETTING_NAME_AD1 )); ?>
  </label>
  <label>
    <input type="radio" name="rotate-cube-side" value="right" /><?php echo (get_option( SETTING_NAME_AD2 )); ?>
  </label>
  <label>
    <input type="radio" name="rotate-cube-side" value="back" /><?php echo (get_option( SETTING_NAME_AD3 )); ?>
  </label>
  <label>
    <input type="radio" name="rotate-cube-side" value="left" /><?php echo (get_option( SETTING_NAME_AD4 )); ?>
  </label>
  <label>
    <input type="radio" name="rotate-cube-side" value="top" /><?php echo (get_option( SETTING_NAME_AD5 )); ?>
  </label>
  <label>
    <input type="radio" name="rotate-cube-side" value="bottom" /><?php echo (get_option( SETTING_NAME_AD6 )); ?>
  </label>
</p>
<?php
} //end if-clause: card or cube
?>
</section> <!-- Ende Cube Sektion -->

<?php 
 } //end if-statement is_home()
} //END PRINT CUBE FUNCTION


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//Optionen hinzufügen in den Admineinstellungen
function plugins_loaded() {
	
	//RadioButton bzw. Dropdown für Karte-/Würfel-Auswahl DERZEIT NICHT VERWENDET ABER FUNKTIONIERT 'GEGENBEISPIEL ZUR USERMETA
if ( false===get_option( SETTING_RADIO_AD )) {
add_option( SETTING_RADIO_AD, SETTING_RADIO_DEFAULT_AD );
}	
	//Textboxen
	//Titel
if ( false===get_option( SETTING_NAME_AD )) {
add_option( SETTING_NAME_AD, SETTING_DEFAULT_AD );
}
	//1-6
if ( false===get_option( SETTING_NAME_AD1 )) {
add_option( SETTING_NAME_AD1, SETTING_DEFAULT_AD1 );
}
if ( false===get_option( SETTING_NAME_AD2 )) {
add_option( SETTING_NAME_AD2, SETTING_DEFAULT_AD2 );
}
if ( false===get_option( SETTING_NAME_AD3 )) {
add_option( SETTING_NAME_AD3, SETTING_DEFAULT_AD3 );
}
if ( false===get_option( SETTING_NAME_AD4 )) {
add_option( SETTING_NAME_AD4, SETTING_DEFAULT_AD4 );
}
if ( false===get_option( SETTING_NAME_AD5 )) {
add_option( SETTING_NAME_AD5, SETTING_DEFAULT_AD5 );
}
if ( false===get_option( SETTING_NAME_AD6 )) {
add_option( SETTING_NAME_AD6, SETTING_DEFAULT_AD6 );
}
}//end plugins_loaded()

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Icon und Einstellungen zum Menübereich hinzufügen
function admin_menu() {
	//base64 encoded svg icon in the menu bar, position 2	//vorher base64 encodieren
	$icon_svg = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAATCAYAAACKsM07AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAMHSURBVEhLtVXPSxRxFP/MzM7MuruiUEpZYaTtoahbF5NoNdyQVRNKUqJT0cGDClKHhP6FIIgCD1KHCKI1hDxEh+oQZLQalrZbSvYLERPbXdyd/TG9951ZN901PdRneQz73sz3vc/nfb/vK/Vev23Ovn0DxaHiXyKVTOBIUzOkY00nzaWvnyErih36N0gbBmrqjkNWdScUVYORzmxqpiTDoelbNA0qPaXGljbze2QaZzu7kE6nYZqmXcNaaPTBxMQEIpEIMpkMYrGYHSmELMsodbuw/2gDpIZAq/njYxjvp6bs8MYIBoPo6elBR0cHBgYGEI/H7UgekiSJIk/4fDjg80Nmp2lmRZArA7LofhmH8+4Sah4sYy6aohfYD6ysrIgFDNKXGRQzThqNRmHSDxLgEF/a4D7fnzFw83UCcMuYSWRQG4zBOF9mvwHouo7R0VEMDQ3ZnkIotNCeXVW0vmQx+BOfosRGpxBHVAmpuMUuh0QigUAggHA4jFAoVGDj4+MYGxsTLJnFugQSHn1JUwnETfwjkDozy5ZEObBMDoejqKmqKkyAVFojEXuuHtLRNkfNc1GzqPjt2xTsK8ufEafTiZGREQwODtqeQqxKxIXYPhsSWqtVHKxS8G6BqiYyw81uO2YhmUwKifr7+4vuIt6i2WwWp9vb8xIplInR9jQO6cYSSkn7Tq+Gi4c11A/HoN9ZxpMF6juVw1vQ4/GgsrISFRUVRY1jXD1LJM7Br9kPuPJ8Gmfu/aRVKMB9zZ03Vodb4HThmfshLly+JpLMz8+LcDFwL3ZX7YS3vjEvkcZc2HKbxiK1msylWQ6WyO/3o6+vb0OJ+Dyd6+oUEokEJtHZWw7UVpfDQ/LkpgWz5GUz9H9HGe3pxRKhL0vg9XpFgvWjhRNYT6JOITGLvoWn0dt9CSUmdXVVm7WQaSA+fvEKk5OTm84i1r+s1GPNosaWU+bi3CwSRH2jQZcDDzy2rSBtJFFb54PMF0MmZUBXHXBq6l9NJnb84dbMQIqedKPdohst9J9utGb8Bhc7fYQeCjeFAAAAAElFTkSuQmCC';
add_menu_page('OHM Info-Cube',
'OHM Info-Cube',
'manage_options',
basename(__FILE__),
array(&$this,
'print_settings_page'),$icon_svg,2
);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Hinzufügen und Registrieren der Einstellungen
function admin_init() {

//Cube: 1. Sektion
add_settings_section(
'cube_settings_frontend',
__('Visitenkarte oder Cube für Ihren Auftritt wählen', DOMAIN),
array(&$this, 'print_settings_frontend'),
'cube_settings'
);
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		
//Cube: 2. Felder
add_settings_field(
SETTING_NAME_AD,
'Ihren Auftritt bearbeiten',
array(&$this, 'print_setting_ad'),
'cube_settings',
'cube_settings_frontend'
);
//Cube: 3. Registrierung
////Karte oder Würfel boolean
register_setting('cube_settings', SETTING_RADIO_AD);

//Titel
register_setting('cube_settings', SETTING_NAME_AD);
 
 //1-6
 register_setting('cube_settings', SETTING_NAME_AD1);
 register_setting('cube_settings', SETTING_NAME_AD2);
 register_setting('cube_settings', SETTING_NAME_AD3);
 register_setting('cube_settings', SETTING_NAME_AD4);
 register_setting('cube_settings', SETTING_NAME_AD5);
 register_setting('cube_settings', SETTING_NAME_AD6);
// //

//Card: 1. Sektion
add_settings_section(
'card_settings_frontend',
'Visitenkarte Sektion',
array(&$this, 'print_settings_frontend_card'),
'card_settings'
);
//Cube: 2. Felder
add_settings_field(
'card_settings', 
'Visitenkarte einstellen', 
array(&$this, 'print_setting_ad_card'), 
'card_settings',
'card_settings_frontend'
);
//Cube: 3. Registrierung
register_setting('card_settings', SETTING_NAME_AD_CARD);

} //end admin_init()


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Nur erlaubte Rollen dürfen im Adminbereich Einstellungen übermitteln, die Anzeige des Buttons selektiert nach Userrollen: Admin und User "root"
function print_settings_page() {
if ( ! current_user_can( 'manage_options' ) ) {
wp_die( 'You do not have sufficient permissions
to manage options for this site.' );
} ?>
<div class="wrap">
<h2><?php echo(esc_html('Settings &rsaquo; Ihre Einstellungen für die Visitenkarte und Informationswürfel der TH Nürnberg')); ?></h2>
<form method="POST" action="options.php"><?php
settings_fields(
'cube_settings' );
//settings_fields('plugin_options');
//do_settings_sections(__FILE__);
do_settings_sections(
'cube_settings' );

//settings_fields('card_settings');
//do_settings_sections('card_settings' );

//falls User eingeloggt
		if ( is_user_logged_in() ) {
			//User
			$user_id = get_current_user_id();
			$user_data = get_userdata($user_id);
			$username = $user_data->user_login;
			//User-Rolle
			$user = wp_get_current_user();
			$allowed_roles = array('editor', 'administrator', 'author');
			//nur der Root-User oder Admins können die Einstellungen speichern, ansonsten Submit-Button verstecken
			if(array_intersect($allowed_roles, $user->roles ) || $username == "root")
			{  
				// Stuff here for allowed roles
				submit_button();
			}
			else {
				//users without plugin settings permission ...
				echo "Please login as root-user for further permissions.";
			}

		}
?></form>
</div><?php
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//DropDown in den eigenen Einstellungen im Adminbereich (DERZEIT NICHT GENUTZT ABER FUNKTIONIERT)
function print_settings_frontend() {
echo '<p>' . 'Ihre aktuelle Darstellungsform: '.'<strong>'.get_option( SETTING_RADIO_AD ).''.'<strong>'.'</p>';

$options = get_option( SETTING_RADIO_AD );
	$items = array("Cube","Card");
	echo '<select 
	name="' . SETTING_RADIO_AD . '"  id="' . SETTING_RADIO_AD . '">';
	//id='drop_down1' name='plugin_options[dropdown1]'>";
	foreach($items as $item) {
		//$selected = ($options[SETTING_RADIO_AD]==$item) ? 'selected="selected"' : '';
		
		if($options[SETTING_RADIO_AD] == $item)
			{
				$selected = "selected";
			}
			else
				$selected = '';
			
		
		//show dropdown list items
		echo "<option value='$item' $selected>$item</option>";
	}
	
	echo "</select>";

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//DropDown ComboBox in den eigenen Einstellungen im Adminbereich
//Hinzufügen und Enkodieren der Textanzeige-Einstellungen (zum Benutzen innerhalb eines <textarea>-Elements)
function print_setting_ad() {

echo '<textarea
name="' . SETTING_NAME_AD . '"
id="' . SETTING_NAME_AD . '">'
. esc_textarea(get_option( SETTING_NAME_AD ))
. '</textarea> ';
?><p class="description"> Den Titel Ihres Auftritts bearbeiten</p><?php

echo '</br>';
echo '</br>';

echo '<textarea
maxlength="12"
name="' . SETTING_NAME_AD1 . '"
id="' . SETTING_NAME_AD1 . '">'
. esc_textarea(get_option( SETTING_NAME_AD1 ))
. '</textarea> ';
?><p class="description"> 1. Textblock - bitte Bearbeitungsfunktion benutzen</p><?php

echo '<textarea
maxlength="12"
name="' . SETTING_NAME_AD2 . '"
id="' . SETTING_NAME_AD2 . '">'
. esc_textarea(get_option( SETTING_NAME_AD2 ))
. '</textarea> ';
?><p class="description"> 2. Textblock - bitte Bearbeitungsfunktion benutzen</p><?php

echo '<textarea
maxlength="12"
name="' . SETTING_NAME_AD3 . '"
id="' . SETTING_NAME_AD3 . '">'
. esc_textarea(get_option( SETTING_NAME_AD3 ))
. '</textarea> ';
?><p class="description"> 3. Textblock - bitte Bearbeitungsfunktion benutzen</p><?php

echo '<textarea
maxlength="12"
name="' . SETTING_NAME_AD4 . '"
id="' . SETTING_NAME_AD4 . '">'
. esc_textarea(get_option( SETTING_NAME_AD4 ))
. '</textarea> ';
?><p class="description"> 4. Textblock - bitte Bearbeitungsfunktion benutzen</p><?php

echo '<textarea
maxlength="12"
name="' . SETTING_NAME_AD5 . '"
id="' . SETTING_NAME_AD5 . '">'
. esc_textarea(get_option( SETTING_NAME_AD5 ))
. '</textarea> ';
?><p class="description"> 5. Textblock - bitte Bearbeitungsfunktion benutzen</p><?php

echo '<textarea
maxlength="12"
name="' . SETTING_NAME_AD6 . '"
id="' . SETTING_NAME_AD6 . '">'
. esc_textarea(get_option( SETTING_NAME_AD6 ))
. '</textarea> ';
?><p class="description"> 6. Textblock - bitte Bearbeitungsfunktion benutzen</p><?php

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Blanko-Funktion für Einstellungen der Visitenkarte im Adminbereich
function print_setting_ad_card () {
	//echo "HALLO";
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///// //////////////////////////////ENDE cube.php = card.php :D
}//End Class cubecard
cubecard::get_instance(); // Create instance

?>