
<!-- ---------------------------------HEADER.PHP---------------------------------------------- -->
<!DOCTYPE html>
<html lang="de-DE">
<head>
<title>project-torpedo.com | </title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<meta name="keywords" content="Projekt Torpedo, Webspace zum Üben" />
<meta name="description" content="Webprogrammierungsprojekt project-torpedo.com" />
<link rel=stylesheet type="text/css" href="project-torpedo.css" media="screen"/> 

<!-- GET http://project-torpedo.com/favicon.ico 404 (Not Found)  ??-->
<link rel="shortcut icon" href="#" />

<!-- Javascript: Funktionen und Webkomponente -->
<script src="grid.js" type="text/javascript"></script>
	<!-- <link rel="import" href="basketPopup.php"/> -->

</head>
    
<body>
<header id="main_header">
   
   <!--Logo -->
  	<a href="index.php">
        <img src="pictures/logo.jpg" id="logo" alt="Logo" title="PROJECT-TORPEDO.COM"/>
    </a> 
 
		
  <!-- H1 Überschrift-->
  <div class="typewriter" id="h1">
		<h1> <strong> PROJECT-TORPEDO.COM  </strong> -  <em>Trainier' wie eine Rakete!</em> </h1>
  </div>
		
</header>
     