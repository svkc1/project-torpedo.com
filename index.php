<?php
include "dbh.php";
?>
<!-- ------------------------------------------------------------------------------- -->
<!DOCTYPE html>
<html lang="de-DE">
<head>
<title>project-torpedo.com | </title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<meta name="keywords" content="Projekt Torpedo, Webspace zum Üben" />
<meta name="description" content="Webprogrammierungsprojekt project-torpedo.com" />
<link rel=stylesheet type="text/css" href="project-torpedo.css" media="screen"/> 

<!-- GET http://project-torpedo.com/favicon.ico 404 (Not Found)  ??-->
<link rel="shortcut icon" href="#" />

<!-- Javascript: Funktionen und Webkomponente -->
<script src="grid.js" type="text/javascript"></script>
	<!-- <link rel="import" href="basketPopup.php"/> -->

</head>
    
<body>
<header id="main_header">
   
   <!--Logo -->
  	<a href="index.php">
        <img src="pictures/logo.jpg" id="logo" alt="Logo" title="PROJECT-TORPEDO.COM"/>
    </a> 
 
		
  <!-- H1 Überschrift-->
  <div class="typewriter" id="h1">
		<h1> <strong> PROJECT-TORPEDO.COM  </strong> -  <em>Trainier' wie eine Rakete!</em> </h1>
  </div>
		
</header>
     
<!-- ----------------------------------------------------------------------------------- -->
<!-- Navigation: ...... -->
<!-- ----------------------------------------------------------------------------------- -->
  
<!-- Navigation der Seite Index-->
<aside>
  <nav>
    <ul>
      <li><a href="seite_1.html">Sport und Fitness</a></li>
      <li><a href="seite_2.html">Unsere App</a></li>
      <li><a href="seite_3.html">Übungen</a></li>
	  <li><a href="seite_4.html">Ernährung</a></li>
	  <li><a href="seite_5.html">Kalorienrechner</a></li>
	  <li><a href="seite_6.html">Infos III</a></li>
	  <li><a href="seite_7.html">Func II</a></li>
<!--  <li><a href="seite_6.html">Unere Traingsapp</a></li>
	  <li><a href="seite_7.html">Kalorienrechner</a></li>
	  <li><a href="seite_8.html">Literatur</a></li>
	  <li><a href="seite_9.html">usw.</a></li>
	  <li><a href="wordpress/">WordPress offline</a></li>
	  <li><a href="wordpress/CMS/">WordPress online</a></li>
-->
	  <!-- NEW REGISTER --> 	
		<form id="newregister_form">
			
			<label>Noch nicht registriert?</label> </br>
			<input id="registerBtn" type="button" value="Jetzt registrieren">
		 </form>
		
	  
	  
	  <!-- LOGIN --> 	
		 <form id="login_form">
			<label>Username:</label>
			<input id="userTB" type="text" name="user">  </br>
			<label>Passwort: </label>
			<input id="passwordTB" type="password" name="password"> 		
			<input id="loginBtn" type="button" name="login" value="Login">		
		  </form>
	  
	  <!-- SUCHE -->
	  <form id="search_form">
		<input id="searchTB" type="text" name="search">
		<input id="searchBtn" type="button" value="Suche"> 
	  </form>
	  
		
    </ul>
	
  </nav>
  

  
</aside>


<!-- ------------------------------------------------------------------------------- -->    
<!-- ------------------------------------------------------------------------------- -->
                            <!-- Inhalt der Seite Impressum -->
<!-- ------------------------------------------------------------------------------- -->
<!-- ------------------------------------------------------------------------------- -->
    
<main>
<section class="banner">
    <p >
	Diese Seite befindet sich aktuell im Aufbau ... 
    </p>
	
	<img src="pictures/tickerleiste.jpg" alt="Nachrichtenleiste" title="Nachrichtenleiste" width=100% height=25px>
</section>	

<!-- ------------------------------------------------------------------------------- -->
  <!-- Slideshow Bereich -->
<!-- ------------------------------------------------------------------------------- -->   
<section class="slider">

 
  <article id="articlegallery">
      <h3>Neue Angebote (Sliderbeispiel)</h3>

      <section id="gallery">
          <figure>
              <img src="pictures/fitnessraum.jpg" alt="Fitnessraum" title="Fitnessraum" width=100%>
          </figure>
          <figure>
              <img src="pictures/frau2.jpg" alt="Frau mit Hantel" title="Frau mit Hantel" width=100%>
          </figure>
          <figure>
              <img src="pictures/hanteln.jpg" alt="Hanteln" title="Hanteln" width=100%>
          </figure>
      </section>

  </article>

</section>


<!-- GRID System-->
<section class=raster>


<!-- Der erste Link -->
<article class=article_raster>
<p>
Kein Bizeps ohne eine ordentliche Hantel:
</p>
<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ac&ref=qf_sp_asin_til&ad_type=product_link&tracking_id=twitchtvmaske-21&marketplace=amazon&region=DE&placement=B00HQG1VK4&asins=B00HQG1VK4&linkId=5d22f7f8f88c7d7af15861dcb18ffe58&show_border=true&link_opens_in_new_window=true&price_color=333333&title_color=0066C0&bg_color=FFFFFF">
    </iframe>
</article>


<!-- ##PHP## -->


<?php
    
        //SQL - meistverkaufte Artikel mit Bestellmenge > 60 	//ta.Massstab, ta.Beschreibung, ta.Listenpreis usw
        $sqlget = "SELECT ta.id_torpedo_artikel, ta.Title, ta.Subtitle, ta.Text
                    FROM torpedo_artikel ta
                    GROUP BY ta.id_torpedo_artikel;";
        $sqldata = $pdo -> query($sqlget);
        
        //Ausgabe aller Datensätze (Artikel) mit foreach-Schleife
        foreach ($sqldata as $row){
         
         //Grid-System aufbauen
		 echo "<article class=article_raster>";
            
                //Artikelüberschrift       
                echo "<h4>";
                echo $row ['Title'];
                echo "</h4>";
                
                //Artikelbeschreibung
                echo "<h5>";
                echo $row ['Subtitle'];
                echo "</h5>";
				
				//Bild
				echo "<figure><img src='pictures/fitnessraum.jpg' alt='Fitnessraum' title='Fitnessraum' width=65% ></figure>";
				
				//Text
                echo "<p>";
                echo $row ['Text'];
                echo "</p>";
                            			
			//Link zur Einzelansicht des Artikels
            echo "<a href='article.php?id_torpedo_artikel={$row ['id_torpedo_artikel']}'> -> Hier Weiterlesen... </a>";
 
		 echo "<section> Amazon API ?! Sektion </section>";
			
		 //Grid-System beenden	
		 echo "</article>";
        }
        
    ?>

</article>

</section>
</main>
    
    <!-- ------------------------------------------------------------------------------- -->
    <!-- ------------------------------------------------------------------------------- -->
                    <!--untere Navigation der Seite Impressum -->

  <footer>
    <ul>
      <!-- <li><a href="about.php">Über uns</a></li>
      <li><a href="#">Impressum</a></li>-->
	  <li><a href="about.html">Über uns</a></li>
      <li><a href="impressum.html">Impressum</a></li>
	  <!-- <li><a href="temp.json">format</a></li>
      <li><a href="#">Sonstiges?! (weitere Fußzeilenlinks)</a></li> -->
    </ul>
  </footer>

</body>
</html>